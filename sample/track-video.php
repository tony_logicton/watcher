<?php

header('Content-Type: application/json');

$post_request = file_get_contents("php://input");
$post_request = json_decode($post_request, true);

$action = empty($post_request['action']) ? null : $post_request['action'];

function add_tracking_data($raw_data) {
    // store to database
}

function rand_id() {
    return substr(md5(microtime() . rand(1, 99)), 0, 10);
}

if ($action == 'get_tracking_data') {

    $response = array(
        'data' => array(
            'unique_id' => rand_id(),
            'video_id' => $post_request['video_id']
        )
    );
}

if ($action == 'push_tracking_data') {

    $raw_data = array(
        'placement_id' => $post_request['placement_id'],
        'placement_banner_code' => $post_request['code'],
        'video_length' => $post_request['video_length'],
        'video_file' => $post_request['video_file'],
        'video_id' => $post_request['video_id'],
        'name' => $post_request['name'],
        'unique_id' => $post_request['video_tracking_data']['unique_id'],
        'view_close' => $post_request['video_tracking_data']['view_close'],
        'view_total' => $post_request['video_tracking_data']['view_total'],
        'timestamp' => date('Y-m-d H:i:s', time())
    );

    add_tracking_data($add_tracking_data);

    $response = array(
        'data' => $raw_data
    );
}

echo json_encode($response);
die;