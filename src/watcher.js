(function () {
    if (typeof window.CustomEvent === "function") return false; function CustomEvent(event, params) {
        params = params || { 
            bubbles: false, 
            cancelable: false, 
            detail: undefined 
        }; 
        var evt = document.createEvent('CustomEvent'); evt.initCustomEvent(
            event, params.bubbles, params.cancelable, params.detail); return evt;
    } CustomEvent.prototype = window.Event.prototype;
    window.CustomEvent = CustomEvent;
})();

var Watcher = function (options) {

    var defaultOptions = {
        inspect: false,
        videoTrackingUrl: ''
    }

    this.options = Object.assign(defaultOptions, options)
    this.element = null
    this.initTimer = new Date()

    this.mouseMoveDuration = 0
    this.mouseHoverDuration = 0
    this.mouseMoveLastTime = null
    this.mouseHoverLastTime = null

    this.videoGap = 5

    this.videoPlayTiming = []
    this.placementBannerCode = window.code
    this.placementId = window.id
    this.videoTrackData = []
}

Watcher.prototype = {
    constructor: Watcher,

    _getElement: function (element) {

        if (this.element) {

            element = this.element
        }

        return typeof element == 'string' ? document.querySelector(element) : element
    },

    _getElementTarget: function (e) {
        var ele

        if (!e) {

            var e = window.event
        }

        if (e.target) {

            target = e.target
        }

        if (e.srcElement) {

            target = e.srcElement
        }

        if (target.nodeType == 3) {

            target = target.parentNode
        }

        return target
    },

    _getMousePositionInDoc: function (e) {
        var posx = 0,
            posy = 0;

        if (!e) {

            var e = window.event
        }

        if (e.pageX || e.pageY) {

            posx = e.pageX
            posy = e.pageY
        }

        if (e.clientX || e.clientY) {

            posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft
            posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop
        }

        return {
            x: posx,
            y: posy
        }
    },

    _getMousePositionInEle: function (e) {

        var mousePosInDoc = this._getMousePositionInDoc(e),
            mousePosInEle = this._getElementTarget(e),
            posx = mousePosInDoc.x - mousePosInEle.offsetLeft,
            posy = mousePosInDoc.y - mousePosInEle.offsetTop

        return {
            posx: posx,
            posy: posy
        }
    },

    _calculateDuration: function (start, end) {

        if (!start || !end) {

            return 0
        }

        var diff = (end.getTime() - start.getTime()) / 1000

        return diff
    },

    _isIframe: function () {
        return window.location.href == window.top.location.href
    },

    _handleThrowError: function (message) {

        var messages = {
            'no_element': 'No video element set',
            'ajax_empty_url': 'Ajax url is required',
            'empty_tracking_video_url': 'Tracking video url is empty',
            'response_format_invalid': 'Response format of tracking video is invalid. Please see README.md'
        }

        throw new Error(messages[message])
    },

    _createXHRRequest: function () {
        var xhr = null

        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest()

        } else {

            if (window.ActiveXObject) {
                try {
                    xhr = new ActiveXObject("Msxml2.XMLHTTP")
                } catch (e) {
                    try {
                        xhr = new ActiveXObject("Microsoft.XMLHTTP")
                    } catch (e) { }
                }
            }
        }

        return xhr
    },

    _ajax: function (options) {

        var xhr = this._createXHRRequest(),
            options = Object.assign({
                success: function () { },
                url: '',
                type: 'get',
                data: {},
                dataType: 'json',
            }, options)

        xhr.addEventListener('load', function () {

            var res = null

            if (options.dataType == 'json') {

                res = {}

                if (this.response.length) {

                    res = JSON.parse(this.response)
                }
            }

            options.success(res, this)
        })

        options.type = options.type.toLowerCase()

        if (!options.url.length) {
            return this._handleThrowError('ajax_empty_url')
        }

        xhr.open(options.type, options.url)

        if (options.type == 'post') {

            options.data = JSON.stringify(options.data)

            return xhr.send(options.data)
        }

        return xhr.send()
    },

    init: function (callback) {

        window.onload = function () {

            if (typeof callback == 'function') {

                callback()
            }

            var event = new window.CustomEvent('watcher_init');
            window.dispatchEvent(event)

            if (this.options.inspect) {

                console.info('Init watcher')
            }

        }.bind(this)

        return this
    },

    mainPageLoaded: function (callback) {

        window.onload = function () {

            if (typeof onMainPageLoaded == 'undefined') {

                window.onMainPageLoaded = function () {

                    callback()
                }
            }
        }

        return this
    },

    parentLoad: function (callback) {

        if (!parent.window) {

            return false
        }

        try {

            parent.window.onload = function () {
                if (typeof callback == 'function') {
                    callback(parent)
                }
            }

            var event = new window.CustomEvent('watcher_parent_load')
            parent.window.dispatchEvent(event)
            window.dispatchEvent(event)

        } catch (e) {

            console.warn('Permission denied to access parent window')
        }

        if (this.options.inspect) {

            console.info('Parent load')
        }

        return this
    },

    setElement: function (ele) {

        this.element = ele
        this.bindEvent()

        return this
    },

    bindEvent: function () {

        if (this.element) {

            var element = this._getElement(),
                self = this

            element.addEventListener('play', function () {

                self.videoPlayTiming.push(new Date())
            })

            element.addEventListener('pause', function () {

                self.videoPlayTiming.push(new Date())
            })

            element.addEventListener('ended', function () {

                self.videoPlayTiming.push(new Date())
                self.videoPlayTiming = []
            })
        }
    },

    onMouseOver: function (element, callback) {

        if (typeof element == 'function' && !callback) {

            callback = element
            element = this._getElement(this.element)
        }

        if (!this.element && typeof element == 'string' && typeof callback == 'function') {

            element = this._getElement(element)
        }

        if (!element) {

            return this
        }

        if (typeof callback == 'function') {

            var self = this

            element.addEventListener('mouseover', function (e) {

                callback(e, self._getMousePositionInEle(e))

                self.mouseHoverLastTime = new Date()

                if (!self.mouseMoveLastTime) {

                    self.mouseMoveLastTime = new Date()
                }

                if (self.options.inspect) {

                    console.info('Mouse over', self._getMousePositionInEle(e))
                }
            })
        }

        return this
    },

    onMouseOut: function (element, callback) {

        if (typeof element == 'function' && !callback) {

            callback = element
            element = this._getElement(this.element)
        }

        if (!this.element && typeof element == 'string' && typeof callback == 'function') {

            element = this._getElement(element)
        }

        if (!element) {

            return this
        }

        if (typeof callback == 'function') {

            var self = this

            element.addEventListener('mouseout', function (e) {

                callback(e, self._getMousePositionInEle(e))

                var hoverDuration = self._calculateDuration(self.mouseHoverLastTime, new Date())
                self.mouseHoverDuration += hoverDuration

                if (self.options.inspect) {
                    console.info('Mouse out', self._getMousePositionInEle(e))
                }
            })
        }

        return this
    },

    onMouseMove: function (element, callback) {

        if (typeof element == 'function' && !callback) {

            callback = element
            element = this._getElement(this.element)
        }

        if (!this.element && typeof element == 'string' && typeof callback == 'function') {

            element = this._getElement(element)
        }

        if (!element) {

            return this
        }

        if (typeof callback == 'function') {

            var self = this

            element.addEventListener('mousemove', function (e) {

                callback(e, self._getMousePositionInEle(e))

                var duration = self._calculateDuration(self.mouseMoveLastTime, new Date())
                self.mouseMoveDuration = duration

                if (self.options.inspect) {
                    console.info('Mouse move', self._getMousePositionInEle(e))
                }
            })
        }

        return this
    },

    onClick: function (element, callback) {

        if (typeof element == 'function' && !callback) {

            callback = element
            element = this._getElement(this.element)
        }

        if (!this.element && typeof element == 'string' && typeof callback == 'function') {

            element = this._getElement(element)
        }

        if (!element) {

            return this
        }

        if (typeof callback == 'function') {

            var self = this

            element.addEventListener('click', function (e) {

                var current = self._getElementTarget(e),
                    counter = current.getAttribute('data-counter')

                if (!counter) {

                    current.setAttribute('data-counter', 1)
                    callback(e, self._getMousePositionInEle(e), 1)

                    return this
                }

                counter = parseInt(counter)
                current.setAttribute('data-counter', counter + 1)

                callback(e, self._getMousePositionInEle(e), counter)

                if (self.options.inspect) {

                    console.info('Mouse click', self._getMousePositionInEle(e))
                }
            })
        }

        return this
    },

    onVideoPlaying: function (element, callback) {

        if (typeof element == 'function' && !callback) {

            callback = element
            element = this._getElement(this.element)
        }

        if (!element) {

            return this
        }

        if (typeof callback == 'function') {

            element.addEventListener('playing', function (e) {

                callback(e, element)
            })
        }

        return this
    },

    onVideoTimeUpdate: function (element, callback) {

        if (typeof element == 'function' && !callback) {

            callback = element
            element = this._getElement(this.element)
        }

        if (!element) {

            return this
        }

        if (typeof callback == 'function') {

            element.addEventListener('timeupdate', function (e) {

                callback(e, element)
            })
        }

        return this
    },

    onVideoPause: function (element, callback) {

        if (typeof element == 'function' && !callback) {

            callback = element
            element = this._getElement(this.element)
        }

        if (!element) {

            return this
        }

        if (typeof callback == 'function') {

            element.addEventListener('pause', function (e) {

                callback(e, element)
            })
        }

        return this
    },

    onVideoEnded: function (element, callback) {

        if (typeof element == 'function' && !callback) {

            callback = element
            element = this._getElement(this.element)
        }

        if (!element) {

            return this
        }

        if (typeof callback == 'function') {

            element.addEventListener('ended', function (e) {

                callback(e)
            })
        }
    },

    getElementShowTime: function () {

        var current = new Date(),
            diff = (current.getTime() - this.initTimer.getTime()) / 1000

        return diff
    },

    getMouseMoveDuration: function () {

        return this.mouseMoveDuration
    },

    getMouseOverDuration: function () {

        return this.mouseHoverDuration
    },

    getVideoPlayedDuration: function () {

        var duration = 0

        for (var i = 0; i < this.videoPlayTiming.length; i++) {

            var next = i + 1

            duration += this._calculateDuration(this.videoPlayTiming[i], this.videoPlayTiming[next])

            i++
        }

        return duration
    },

    openFullscreen: function (element, callback) {

        if (typeof element == 'function' && !callback) {

            callback = element
            element = this._getElement(this.element)
        }

        if (!element) {

            return this
        }

        var isMozFullScreen = document.mozFullScreen,
            isWebkitFullScreen = document.webkitFullScreen

        if (!isMozFullScreen && !isWebkitFullScreen) {

            if (element.mozRequestFullScreen) {

                element.mozRequestFullScreen()
            } else {

                element.webkitRequestFullScreen()
            }

            var event = new window.CustomEvent('watcher_open_fullscreen')

            parent.window.dispatchEvent(event)
            window.dispatchEvent(event)

            callback()
        }

        return this
    },

    getElementWidth: function (element) {

        if (this.element) {

            element = this._getElement(this.element)
        }

        if (!element) {

            return this
        }

        return element.offsetWidth
    },

    getElementHeight: function (element) {

        if (this.element) {

            element = this._getElement(this.element)
        }

        if (!element) {

            return this
        }

        return element.offsetHeight
    },

    getParentIframe: function () {

        var self = window.location,
            path = self.pathname.replace('/', ''),
            currentIframe = parent.document.querySelector('iframe[src*="' + path + '"]')

        return {
            e: currentIframe,
            width: currentIframe.offsetWidth,
            height: currentIframe.offsetHeight,
            x: currentIframe.offsetLeft,
            y: currentIframe.offsetTop,
        }
    },

    _handleTrackVideo: function (element) {

        var self = this,
            videoId = element.getAttribute('id')

        this._getVideoTrackingData(videoId, function (res) {
            self._setupTrackingData(self._sanitizeTrackingData(res.data))
            self._startTracking(element)
        })
    },

    _setupTrackingData: function (sanitizedTrackingData) {

        if (!sanitizedTrackingData) {
            return
        }

        var index = this.videoTrackData.findIndex(function (trackingData) {
            return trackingData.id == sanitizedTrackingData.id
        })

        if (index > -1) {
            this.videoTrackData[index] = sanitizedTrackingData
            return
        }

        this.videoTrackData.push(sanitizedTrackingData)
    },

    _handleStopTrackVideo: function (video) {

        var videoId = video.getAttribute('id'),
            videoName = video.getAttribute('data-name'),
            trackingData = this._findTrackingDataById(videoId),
            clientId = trackingData.uniqueId ? trackingData.uniqueId : 0,
            videoFile = video.currentSrc,
            trackingData = {
                unique_id: clientId,
                tracking_time: 1,
                view_start: 1,
                view_close: 1,
                view_total: video.currentTime
            }

        this._pushTrackingData(videoId, video.duration, videoFile, trackingData, videoName)
        video.pause()
    },

    _sanitizeTrackingData: function (trackingData) {

        if (!trackingData) {

            this._handleThrowError('response_format_invalid')
            
            return {
                uniqueId: 0,
                id: 0
            }
        }

        return {
            uniqueId: trackingData.unique_id,
            id: trackingData.video_id
        }
    },

    _getVideoTrackingData: function (videoId, callback) {

        if (!this.options.videoTrackingUrl.length) {
            return this._handleThrowError('empty_tracking_video_url')
        }

        this._ajax({
            url: this.options.videoTrackingUrl,
            type: 'post',
            data: {
                placement_id: this.placementId,
                video_id: videoId,
                action: 'get_tracking_data',
                code: this.placementBannerCode
            },
            success: function (res) {
                callback(res)
            }
        })
    },

    _getVideoTimeRange: function (duration) {
        var timeDurationRanges = [],
            duration = Math.ceil(duration)

        for (var i = this.videoGap; i <= duration; i += this.videoGap) {
            timeDurationRanges.push(i)
        }

        return timeDurationRanges
    },

    _handleTimeUpdate: function (video) {

        var currentTime = Math.floor(video.currentTime),
            timeGaps = this._getVideoTimeRange(video.duration),
            videoId = video.getAttribute('id'),
            videoName = video.getAttribute('data-name'),
            videoFile = video.currentSrc

        video.setAttribute('data-current', video.currentTime)

        if (timeGaps.indexOf(currentTime) > -1) {

            if (currentTime != video.getAttribute('data-pass')) {

                var trackingData = this._findTrackingDataById(videoId),
                    clientId = trackingData.uniqueId ? trackingData.uniqueId : 0,
                    trackingData = {
                        unique_id: clientId,
                        tracking_time: 1,
                        view_start: 1,
                        view_close: 0,
                        view_total: video.currentTime
                    }

                this._pushTrackingData(videoId, video.duration, videoFile, trackingData, videoName)
            }

            video.setAttribute('data-pass', currentTime)
        }
    },

    _pushTrackingData: function (videoId, videoLength, videoFile, trackingData, videoName) {

        if (!this.options.videoTrackingUrl.length) {
            return this._handleThrowError('empty_tracking_video_url')
        }

        this._ajax({
            url: this.options.videoTrackingUrl,
            type: 'post',
            data: {
                video_id: videoId,
                name: videoName,
                placement_id: this.placementId,
                action: 'push_tracking_data',
                code: this.placementBannerCode,
                video_tracking_data: trackingData,
                video_length: videoLength,
                video_file: videoFile
            },
            success: function (res) { }
        })
    },

    _startTracking: function (element) {

        var self = this

        if (!element) {
            this._handleThrowError('no_element')
        }

        this.onVideoTimeUpdate(element, function (e, video) {
            self._handleTimeUpdate(video)
        })
    },

    _findTrackingDataById: function (id) {

        return this.videoTrackData.find(function (trackingData) {
            return trackingData.id == id
        })
    },

    trackVideo: function (element) {

        if (this.element) {

            element = this._getElement(element)
        }

        if (typeof element == 'string' && !element.length) {

            this._handleThrowError('no_element')

            return this
        }

        if (!this.element && typeof element == 'string' && element.length) {

            element = this._getElement(element)
        }

        if (!element) {

            this._handleThrowError('no_element')

            return this
        }

        this._handleTrackVideo(element)

        return this
    },

    stopTrackVideo: function (element) {

        if (typeof element == 'string' && !element.length) {

            this._handleThrowError('no_element')

            return this
        }

        if (!this.element && typeof element == 'string' && element.length) {

            element = this._getElement(element)
        }

        if (!element) {

            return this
        }

        this._handleStopTrackVideo(element)

        return this
    }
}