# Watcher Documentation

## Get Started

```
<video id="video-banner">
    <source src="video.mp4"></source>
</video>

<script src="src/watcher.js">

var watcher = new Watcher()
watcher.setElement('#video-banner')
    .onClick(function (e) {
        // Do something when click
    })
    .onMouseMove(function (e) {
        // Do something when mouse move
    });
```

## Methods

**.mainPageLoaded(callback)**

method for do something after main page loaded

```
var watcher = new Watcher()

watcher.mainPageLoaded(function() {
    
    setTimeout(function() {
        video_obj.play()
    }, 3000)
})

```
see sample in `sample/main-page-loaded.html`

**.setElement(element)**

method for set element to watch accept both string and DOM Object

```
var watcher = new Watcher()
watcher.setElement('#video-banner')
watcher.setElement(document.getElementById('video-banner')
```

**.onClick(e, pos, counter)**

method for trigger when click element and callback function accept e is event, pos is position of element that clicked and counter is number of clicking

```
var watcher = new Watcher()

watcher.setElement('#video-banner')
    .onClick(function (e, pos, counter) {
        //
    })
    
watcher.onClick('#video-banner', function (e, pos, counter) {
})
```

**.onMouseOver(e, pos)**

method for trigger when mouse over element and callback function accept e is event, pos is position cursor on element

```
var watcher = new Watcher()

watcher.setElement('#video-banner')
    .onMouseOver(function (e, pos) {
        //
    })
    
watcher.onClick('#video-banner', function (e, pos) {
})
```

**.onMouseOut(e, pos)**

method for trigger when mouse out of element and callback function accept e is event, pos is position cursor on element

```
var watcher = new Watcher()

watcher.setElement('#video-banner')
    .onMouseOut(function (e, pos) {
        //
    })
    
watcher.onMouseOut('#video-banner', function (e, pos) {
})
```

**.onMouseMove(e, pos)**

method for trigger when mouse move on element and callback function accept e is event, pos is position cursor on element

```
var watcher = new Watcher()

watcher.setElement('#video-banner')
    .onMouseMove(function (e, pos) {
        //
    })
    
watcher.onMouseMove('#video-banner', function (e, pos) {
})
```

**.onVideoPlaying(e)**

method for trigger when video banner is playing, callback function accept e is element event

```
var watcher = new Watcher()

watcher.setElement('#video-banner')
    .onVideoPlaying(function (e, pos) {
        //
    })
    
watcher.onVideoPlaying('#video-banner', function (e, pos) {
})
```

**.onVideoPause(e)**

method for trigger when video banner is pause, callback function accept e is element event

```
var watcher = new Watcher()

watcher.setElement('#video-banner')
    .onVideoPause(function (e, pos) {
        //
    })
    
watcher.onVideoPause('#video-banner', function (e, pos) {
})
```

**.onVideoEnded(e)**

method for trigger when video banner is ended, callback function accept e is element event

```
var watcher = new Watcher()

watcher.setElement('#video-banner')
    .onVideoEnded(function (e, pos) {
        //
    })
    
watcher.onVideoEnded('#video-banner', function (e, pos) {
})
```

**.openFullscreen(e)**

method open element as fullscreen

```
var watcher = new Watcher()

watcher.setElement('#video-banner')
    .openFullscreen(function (e, pos) {
        //
    })
    
watcher.openFullscreen('#video-banner', function () {
})
```

**.getElementShowTime()**

method getting duration of element loaded until now return as second

```
var watcher = new Watcher()

watcher.setElement('#video-banner')
    .getElementShowTime()  
})
```

**.getMouseMoveDuration()**

method getting duration mouse moving on element return as second

```
var watcher = new Watcher()

watcher.setElement('#video-banner')
    .getMouseMoveDuration()
})
```

**.getMouseOverDuration()**

method getting duration of mouse on element return as second

```
var watcher = new Watcher()

watcher.setElement('#video-banner')
    .getMouseOverDuration()
})
```

**.getVideoPlayedDuration()**

method getting duration that video played return as second

```
var watcher = new Watcher()

watcher.setElement('#video-banner')
    .getVideoPlayedDuration()
})
```

**.getElementWidth()**

method getting width of element

```
var watcher = new Watcher()

watcher.setElement('#video-banner')
    .getElementWidth()
})
```

**.getElementHeight()**

method getting height of element

```
var watcher = new Watcher()

watcher.setElement('#video-banner')
    .getElementHeight()
})
```

**.getParentIframe()**

method getting parent iframe info but parent window should allow cross origin

```
var watcher = new Watcher()

watcher.parentLoad(function () {
    watcher.getParentIframe()
})
```

**.trackVideo('#video_id')**

method for start track video, using this method, we need to add `videoTrackingUrl`

```
<video id="video_obj"></video>


var watcher = new Watcher({
    videoTrackingUrl: '/ajax/video_tracking.php'
})
watcher.trackVideo('#video_obj')
```

**.stopTrackVideo('#video_id')**

method for stop track video, using this method, we need to add `videoTrackingUrl`

```
<video id="video_obj"></video>


var watcher = new Watcher({
    videoTrackingUrl: '/ajax/video_tracking.php'
})
watcher.stopTrackVideo('#video_obj')
```

## Events

```
// Event triggered when window is load
window.addEventListener('watcher_init', function() {
    console.log('Log init by event listener')
})

// Event triggered when parent window is load, in case loaded on iframe
window.addEventListener('watcher_parent_load', function() {
    console.log('Log when parent loaded by listener')
})

// Event triggered when element open as fullscreen
window.addEventListener('watcher_open_fullscreen', function() {
    console.log('Log when open full screen')
})
```